package com.kartal.mongodemo;

import com.kartal.mongodemo.model.Student;
import com.kartal.mongodemo.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MongoDemoApplicationTests {

	@Autowired
	private StudentRepository studentRepository;

	@Test
	void insertTest() {
		Student student = new Student();
		student.setId("1");
		student.setName("kartal");
		student.setLessons(List.of("math", "music"));
		studentRepository.save(student);
	}

}
