package com.kartal.mongodemo.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;


@Document(collection = "Students")
@Data
public class Student {

    @Id
    private String id;
    private String name;
    private String surname;
    List<String> lessons = new ArrayList<>();
}
