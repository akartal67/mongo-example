package com.kartal.mongodemo.repository;

import com.kartal.mongodemo.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<Student, String> {

}
